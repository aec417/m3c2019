! Lab 4, task 1

program task1
    implicit none
    integer :: i1, solution
    integer, parameter :: N=5, display_iteration = 1
    real(kind=8) :: odd_sum

    call computeSum(N,display_iteration,solution)
	print*, 'sol=',solution
end program task1

subroutine computeSum(N,display_iteration,solution)
	implicit none
	integer:: N, display_iteration, solution
    integer :: i1
    real(kind=8) :: odd_sum
	
	odd_sum = 0.d0	

	do i1 = 1,N
		odd_sum=odd_sum+(1+(2*(i1-1)))
		if (display_iteration==1) then
			print*, 'Sum=',odd_sum
		end if
	end do 
	solution=odd_sum	
end subroutine computeSum
! To compile this code:
! $ gfortran -o task1.exe lab41.f90
! To run the resulting executable: $ ./task1.exe
